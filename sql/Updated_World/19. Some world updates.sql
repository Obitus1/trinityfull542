-- Add Rest of Items & Proper COST to the vendor Tan Shin Tiao
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 87548, 0, 0, 3890, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 87549, 0, 0, 3890, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 89363, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 89795, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 93230, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 104198, 0, 0, 5229, 1);

-- Fix up the item in Tan Shin Tiao to add REP REQUIREMENT
UPDATE item_template SET RequiredReputationFaction = 1345 where entry = 104198;
UPDATE item_template SET RequiredReputationRank = 7 where entry = 104198;

-- Add the Horn to the NPC, should have 98% dropchance sungraze behemoth
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (58895, 89682, 98, 1, 0, 1, 1);

-- And Add Thaft of Fur to the npc shoul dalso have 98 drop chance
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (66587, 89770, 98, 1, 0, 1, 1);

-- Also add the loot to 69768/zandalari-warscout
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94159, 30, 1, 0, 1, 1);
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94227, 25, 1, 0, 1, 1);
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94225, 25, 1, 0, 1, 1);
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94226, 25, 1, 0, 1, 1);
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94223, 25, 1, 0, 1, 1);
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (69768, 94158, 15, 1, 0, 1, 1);

-- Spawn Missing NPC Raider Bork <War Mount Quartermaster>
REPLACE INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES (161195, 12796, 1, 1637, 5170, 1, 1, 0, 0, 1642.14, -4232.46, 52.2176, 4.4855, 120, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL);

-- Add missing mounts to vendor Old Whitenose <Dragon Turtle Breeder>
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 2, 91007, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 7, 91008, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 6, 91009, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 13, 91011, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 10, 91012, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 12, 91013, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 11, 91014, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 9, 91015, 0, 0, 0, 1);


-- Add remaining missing mounts to Gina Mudclaw, 58706
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89362, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89390, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89391, 0, 0, 0, 1);
