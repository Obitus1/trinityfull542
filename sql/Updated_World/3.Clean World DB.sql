-- Clean DB
DELETE FROM npc_vendor WHERE ExtendedCost=9999999;
DELETE FROM npc_vendor WHERE entry NOT IN (SELECT entry FROM creature_template);
DELETE from npc_vendor where entry in (69318,69322);
DELETE FROM npc_vendor WHERE item IN (22053,43230,43232,43234,30637,25849,102463,44939,47499,58149,107951,109013,37100,17967,36915,37706,42083);


DELETE FROM spell_group WHERE spell_id IN (52109,496);
DELETE FROM item_script_names WHERE Id=1031313;
-- Item (entry=25441) in creature_equip_template.itemEntry1 for entry = 41260 is not equipable in a hand, forced to 0.
DELETE FROM creature_equip_template WHERE entry=500012;
DELETE FROM creature_template WHERE entry in (3168497,4000003,4000007,3568497,3368497,3468497,4100001,4100005,4100006,4100007);
UPDATE creature_template SET difficulty_entry_4=0 WHERE entry=68497;

UPDATE creature_template SET equipment_id=0 WHERE equipment_id NOT IN ( SELECT entry FROM creature_equip_template );
UPDATE creature SET equipment_id=0 WHERE equipment_id NOT IN ( SELECT entry FROM creature_equip_template );
-- UPDATE creature SET spawnMask=1 WHERE spawnMask=15;
UPDATE creature_template SET unit_class=1 WHERE unit_class=0;

-- Disable flags for map are invalid, skipped.
UPDATE disables SET flags=31 WHERE sourceType=2 AND entry=616;

-- Table `creature` have creature (GUID: xx Entry: xx) with `phaseMask`=0 (not visible for anyone)
UPDATE creature SET phaseMask=1 WHERE phaseMask=0;

-- with `MovementType`=1 (random movement) but with `spawndist`=0, replace by idle movement type (0).
UPDATE creature SET MovementType=0 WHERE spawndist=0;
UPDATE creature SET spawndist=0 WHERE spawndist<>0 and MovementType=0;	


DELETE FROM creature_model_info WHERE modelid IN (15713,51988,51990,51993,51994,53038);



UPDATE creature_template set npcflag=129 where entry=5870;

DELETE FROM creature WHERE guid IN (8846214,8846219,8846231,8846234,8858254);
UPDATE gameobject SET state=0 WHERE id IN (212975,212976,212162);

DELETE FROM creature_queststarter WHERE id=4100001;
DELETE FROM creature_questender WHERE id=4100001;


DELETE FROM battle_pet_item_to_species WHERE itemId IN (49664,64439,100908,106240,106244,106256,108438,109014);
DELETE FROM smart_scripts WHERE entryorguid IN (-6537184,-501992,-501991,-127497,-127496,-127493,-121192,-118429,-118427,-118425,
-118422,-118421,-118408,-118407,-111383,-111382,-111380,-111379,-111378,-111377,-109655,-109654,-109653,-109652,-98576,-98575,-45287,-44900);
DELETE FROM creature_text WHERE emote=100;
DELETE FROM creature_text WHERE sound=19855;
DELETE FROM script_waypoint WHERE entry=17876;


UPDATE smart_scripts SET event_flags=1 WHERE entryorguid=3835 AND event_type=6;
UPDATE smart_scripts SET event_flags=1 WHERE entryorguid=7872;
UPDATE smart_scripts SET event_flags=1 WHERE entryorguid=3631;
DELETE FROM smart_scripts WHERE entryorguid in (4462900,666666);
DELETE FROM smart_scripts WHERE event_type=77;

DELETE FROM smart_scripts WHERE action_param1 IN (90933,90947,90939,90955,90961,
90952,90957,90959,90931,93386,93385,93389,93388,93387,93383,90921,90918,90930,
91039,92942,91006,92947,91063,91010,92953,90982,90981,90979,93359,93360,93355,93350,93344,
93343,93353,96767,95768,95769,92941,90022,89992,90024,89990,95676,90027,89988,93134,93132,
93133,90038,89999,93177,93180,93175,93178,93176,93179,95524,91931,72671,66963,91899,
70322,75331,70387,73046,70400,70184,70183,70182,70275,70283,70282,70281,70271,70273,
70276,70277,70285,70280,70279,70269,70270,73076,70205,70213,70208,70207,70206,68964,
70212,70209,67886,67879,67880,12296,95767,16858,30453,30443,30442,30437) AND action_type=11;

DELETE FROM smart_scripts WHERE action_type=85 AND action_param1 IN (30437,30442,30443,30453);

UPDATE smart_scripts SET target_param1=50000 WHERE action_type=11 AND target_type=21 AND entryorguid=29775;
UPDATE smart_scripts SET event_flags=0 WHERE entryorguid=42328;

DELETE FROM disables WHERE sourceType=1 AND entry IN (7666,7667,7668,7669,7670,7673,7674,7675,7676);
UPDATE disables SET flags=0 WHERE sourceType=1 AND entry IN (30188,31810,31811,32399,32401,32455,32617);

-- New Clean
UPDATE gameobject_template SET data1=0 WHERE TYPE=0 AND data1>5000000;
UPDATE gameobject_template SET data0=0 WHERE TYPE=8 AND data0>5000000;
UPDATE gameobject_template SET data12=0 WHERE TYPE=10 AND (entry>=213571 AND entry<=213591);
DELETE FROM creature WHERE id NOT IN (SELECT entry FROM creature_template);


-- 30/09/2015 Clean
UPDATE gameobject_template SET data1=0 WHERE TYPE=0 AND data1=-1;
-- GoType: 22) have data0=0 but Spell (Entry 0) not exist.
UPDATE gameobject_template SET data0=61360 WHERE entry=208325;
-- remove error DB
UPDATE gameobject_template SET data0=1 WHERE entry IN (204422,205876,205877,207073,207078);
-- but expected boolean (0/1) noDamageImmune field value
UPDATE gameobject_template SET data11=0 WHERE entry=210935;
-- listed in `spell_proc_event` probally not triggered spell
DELETE FROM spell_proc_event WHERE entry=145378;
-- Clean Gameobject
DELETE FROM gameobject WHERE id NOT IN (SELECT entry FROM gameobject_template);
DELETE FROM gameobject_queststarter WHERE id NOT IN (SELECT entry FROM gameobject_template);
DELETE FROM gameobject_questender WHERE id NOT IN (SELECT entry FROM gameobject_template);
-- Creature (Entry: 68497) has `difficulty_entry_3`=3168497 but creature entry 3168497 does not exist.
UPDATE creature_template SET difficulty_entry_3=0 WHERE entry=68497;
-- 68497) has non-existing faction_A/H template (0).
UPDATE creature_template SET faction_A=14,faction_H=14 WHERE entry IN (68497,61450);



-- entry xxx with `ScriptName` filled in. `ScriptName` of difficulty 0 mode creature is always used instead.
UPDATE creature_template SET ScriptName='' WHERE entry IN 
(3164368,3169465,150112,60048,150159,3160999,150142,3162983,3167977,48913,150085,150167,3169314,64190,3162995,150155,77013,15996,15997,150115,150127,
150168,150087,3161046,150160,3160586,150165,3162919,150081,3169480,33147,150166,77029,3169712,150114,3369314,3364368,3369465,3360999,3362983,3367977,
3369314,3362995,77057,3361046,3160885,3360586,3362919,77068,3369712,3169153,150126,3166100,77028,3170252,150102,3162442,150088,49108,3161038,77063,
60044,60433,150145,150131,150134,3160583,150116,150080,150161,150144,150128,150157,150162,150163,150148,150164,150086,150146,150089,150156,150097,
150098,150099,150100,60144,150152,150154,150170,150103,150105,150107,150108,150109,150110,150133,150130,150138,59916,60052,60091,150141,77028,
3160585,3160788,3160957,3160957,3160957,61335,77014,77015,77016,77104,77062,61990,150119,3162969,3162977,3164390,364520,3164846,150135,150140,
3169911,150169,3169548,3170212,3170235,3170247,77028,3264368,3269465,3260999,3262983,3267977,3269314,3262995,77100,3261046,3360885,3260586,
3262919,3369480,77111,3269712,3369153,3366100,3370252,3362442,3361038,77106,3360583,3360585,3360788,3360957,3161034,3161042,77058,77059,77060,
77105,3362969,3362977,3364390,3364846,3369548,3370212,3370235,3370247,3464368,3469465,3460999,3462983,3467977,3469314,3462995,3461046,3260885,3460586,
3462919,3269480,3469712,3269153,3266100,3270252,3262442,3261038,3260583,3260585,3260788,3260957,3361034,3361042,77101,77102,77103,3262969,3262977,
3264390,3264846,3269548,3270212,3270235,3270247,3564368,3569465,3560999,3562983,3567977,3569314,3562995,3561046,3460885,3560586,3562919,3469480,3569712,
3466100,3470252,3462442,3461038,3460583,3460585,3460788,3460957,3261034,3261042,3462969,3462977,3464390,3464846,3469548,3470212,3470235,
3470247,3560885,3569480,3566100,3570252,3562442,3561038,3560583,3560585,3560788,3560957,3461034,3461042,3562969,3562977,3564390,3564846,
3569548,3570212,3570235,3570247,3561034,3561042,3569153,77067,77110
);
-- List non exist creature in difficulty mode
UPDATE creature_template SET difficulty_entry_6=0 WHERE entry=60381;
UPDATE creature_template SET difficulty_entry_6=0 WHERE entry IN (68497,69153,77094);
UPDATE creature_template SET difficulty_entry_4=0 WHERE entry=70587;
UPDATE creature_template SET difficulty_entry_5=0 WHERE entry IN (60009,77094);
UPDATE creature_template SET difficulty_entry_7=0 WHERE entry=68497;


UPDATE creature_template SET AIName='' WHERE entry IN (
109000,3169916,3170589,109001,3170153,109027,3169352,3170587,109017,109021,109007,110033,109006,109008,109016,109018,109002,3169911,3170594,
3170586,3369916,3370589,3369352,3370586,110032,3370153,109009,109020,3169351,3369911,3370594,3269916,3270589,3269352,3270586,3270153,3369351,
3269911,3270594,3470153,3570153,3270587,3470587,3569916,3570589,3569352,3570586,3469351,3569911,3570594,3569351,3469916,3470589,3469352,3470586,
3269351,3570587,3469911,3470594
);

-- SOme creature has non exist modelid, add default ugly modelid for them LOL
UPDATE creature_template SET modelid1=13 WHERE modelid1=0;
UPDATE creature_template SET modelid1=13 WHERE entry IN (74413,74405,74402,74410,76084);

-- has non exist faction Template, add default for them
UPDATE creature_template SET faction_A=14,faction_H=14 WHERE entry IN (3268497,61550,61447,61449,61549,61551);

-- Missing base stats for creature class 1 level 0
DELETE FROM creature_classlevelstats WHERE LEVEL=0;
INSERT INTO creature_classlevelstats VALUES 
(0,1,1,1,1,1,1,0,0);

-- NPC has different Flags
UPDATE creature_template SET npcflag=0 WHERE entry IN (22684,22730,22734,22733,22712,22528,22699,32043,31947,31951,31950,32049,31924,31963,21599,20912,21601);
UPDATE creature_template SET npcflag=1 WHERE entry IN (61391);
UPDATE creature_template SET npcflag=3 WHERE entry IN (31982,31826,31994);


-- Creature (Entry: 70153) has `difficulty_entry_6`=3470153 but creature entry 3470153 has itself a value in `difficulty_entry_3`.
UPDATE creature_template SET difficulty_entry_3=0 WHERE entry IN (3570587,3470153,3170153,3170586,3570153,3470586,3170587,3270153,3270586,3570586,3370153,3370586,3270587,3470587);
UPDATE creature_template SET difficulty_entry_4=0 WHERE entry IN (77067,77110);
UPDATE creature_template SET difficulty_entry_7=0 WHERE entry IN (77014,77058,77101);
UPDATE creature_template SET difficulty_entry_4=0 WHERE difficulty_entry_4=77029;

--  Has different unit_class
UPDATE creature_template SET unit_class=2 WHERE entry IN (77006,3169455,77049,3369455,77090,3269455,60439,3469455,3569455);

-- Well, does not need this to proc Arcane Charge 36032 - Arcane Missiles! 79683 - Stampede 81022
DELETE FROM spell_proc WHERE SpellID IN (36032,79683,81022);
-- Unknow Spell
DELETE FROM spell_proc WHERE SpellID IN (122016,79683,145151,145152);

-- Wrong spawnMask ( Zul Gurub is Ins 85 Hc only
UPDATE creature SET spawnMask=2 WHERE id IN (15091);

-- Wrong spawnMask
UPDATE creature SET spawnMask=24 WHERE guid IN (135787,136432,136446);
UPDATE creature SET spawnMask=1 WHERE (guid>=989427 AND guid<=989432) AND map=628;
UPDATE creature SET spawnMask=120 WHERE guid IN (986220,986221,200988,201020);
UPDATE creature SET spawnMask=24 WHERE guid IN (986377,986378);
UPDATE creature SET spawnMask=2 WHERE guid IN (986478,986372,986374,986371,986272,986273,986274,986275,986276,986242,986244,986245,986158,319424,404370,930688,930689);
UPDATE creature SET spawnMask=6 WHERE guid IN (986299,986213,930690,930691,402670,402671,404111,404112,404766,404767,404768);
UPDATE creature SET spawnMask=16 WHERE guid IN (700000,792174);
UPDATE creature SET spawnMask=248 WHERE guid IN (404365);
UPDATE creature SET spawnMask=512 WHERE guid IN (408328,930643);
UPDATE creature SET spawnMask=262 WHERE (guid>=900000 AND guid<=900332) AND map=1011;
UPDATE creature SET spawnMask=6 WHERE (guid>=989505 AND guid<=989518) AND map=585;
UPDATE creature SET spawnMask=2 WHERE (guid>=989579 AND guid<=989655) AND map=389;
UPDATE creature SET spawnMask=120 WHERE (guid>=997324 AND guid<=997377) AND map=631;
UPDATE creature SET spawnMask=8 WHERE (guid>=1524110 AND guid<=1524141) AND map=532;
UPDATE creature SET spawnMask=248 WHERE (guid>=7352400 AND guid<=7352453) AND map=1009;
UPDATE creature SET spawnMask=248 WHERE (guid>=8900856 AND guid<=8901131) AND map=1008;
UPDATE creature SET spawnMask=262 WHERE (guid>=8901431 AND guid<=8901744) AND map=961;
UPDATE creature SET spawnMask=2 WHERE (guid>=986252 AND guid<=986270) AND map=329;
UPDATE creature SET spawnMask=2 WHERE (guid>=986942 AND guid<=986945) AND map=329;
UPDATE creature SET spawnMask=16632 WHERE (guid>=8304509 AND guid<=8304511) AND map=1136;
UPDATE creature SET spawnMask=6 WHERE (guid>=986296 AND guid<=986298) AND map=556;


DELETE FROM linked_respawn WHERE guid IN (126434,126435,126437,126438);

UPDATE gameobject SET spawnMask=248 WHERE (guid>=6619530 AND guid<=6619563) AND map=1098;

UPDATE gameobject SET spawnMask=262 WHERE spawnMask=15 AND map=961;

UPDATE gameobject SET spawnMask=248 WHERE spawnMask=760 AND map=1008;

UPDATE gameobject SET spawnMask=248 WHERE spawnMask=15 AND map=1009;

UPDATE gameobject SET spawnMask=262 WHERE spawnMask=15 AND map=1011;

UPDATE gameobject SET spawnMask=2 WHERE spawnMask=3 AND map=389;

UPDATE gameobject SET spawnMask=6 WHERE spawnMask=3 AND map=595;

UPDATE gameobject SET spawnMask=2 WHERE spawnMask=1 AND map=230;

UPDATE gameobject SET spawnMask=6 WHERE spawnMask=3 AND map=560;

UPDATE gameobject SET spawnMask=2 WHERE spawnMask=1 AND map=309;

UPDATE gameobject SET spawnMask=1 WHERE spawnMask=4 AND map=870;

UPDATE gameobject SET spawnMask=2 WHERE spawnMask=1 AND map=43;

UPDATE gameobject SET spawnMask=120 WHERE spawnMask=15 AND map=631;
UPDATE gameobject SET spawnMask=2 WHERE guid=20911;
UPDATE gameobject SET spawnMask=8 WHERE guid=56429;
UPDATE gameobject SET spawnMask=1 WHERE guid=60300;
UPDATE gameobject SET spawnMask=6 WHERE guid=65564;
UPDATE gameobject SET spawnMask=120 WHERE guid=151192;
UPDATE gameobject SET spawnMask=6 WHERE guid=173906;
UPDATE gameobject SET spawnMask=120 WHERE guid=192720;
UPDATE gameobject SET spawnMask=6 WHERE guid=6604820;
UPDATE gameobject SET spawnMask=248 WHERE guid=6619565;

-- Remove error log for command
DELETE FROM command WHERE NAME IN
('reload creature_summon_groups','reload creature_summon_groups','reload creature_questender',
'reload creature_involvedrelation','spectate','start','premium','premium arena','premium changefaction',
'premium changerace','premium deserter','premium mall','reset specialization','reload creature_questrelation',
'reload achievement_criteria_data','modify honor','pet','modify talentpoints','pet create','pet learn','pet unlearn',
'mmap path','mmap stats','mmap testarea','mmap loadedtiles','mmap loc','lfg player','lfg queue','lfg clean','lfg group',
'lfg options','group','group disband','group join','group leader','group list','group remove','group summon','deserter bg add',
'deserter bg remove','deserter instance add','deserter instance remove','disable add mmap','disable remove mmap','reload gameobject_involvedrelation',
'reload gameobject_questender','reload gameobject_questrelation','reload gameobject_queststarter','reload gameobject_scripts',
'reload quest_end_scripts','reload quest_start_scripts','reload teleporter_menu','reload spell_bonus_data','spectate leave',
'account lock country','debug phase','maxskill','character getrename','learn all my pettalents','reload creature_queststarter',
'groupsummon','gobject activate','modify gmspeed','reset all','modify speed backwalk'
);

-- has invalid gameobject (GUID: xxxxx ) in SCRIPT_COMMAND_RESPAWN_GAMEOBJECT for script id xx
DELETE FROM quest_end_scripts WHERE datalong IN (15175,15176) AND id=63;
DELETE FROM quest_end_scripts WHERE datalong IN (35875) AND id=308;
DELETE FROM quest_end_scripts WHERE datalong IN (6552) AND id=902;
DELETE FROM quest_end_scripts WHERE datalong IN (30276,42936,67984) AND id=931;
DELETE FROM quest_end_scripts WHERE datalong IN (48878) AND id=996;
DELETE FROM quest_end_scripts WHERE datalong IN (48879) AND id=998;
DELETE FROM quest_end_scripts WHERE datalong IN (15175,15176) AND id=1103;
DELETE FROM quest_end_scripts WHERE datalong IN (13621) AND id=1191;
DELETE FROM quest_end_scripts WHERE datalong IN (48880) AND id=1514;
DELETE FROM quest_end_scripts WHERE datalong IN (48876) AND id=2523;
DELETE FROM quest_end_scripts WHERE datalong IN (48877) AND id=2878;
DELETE FROM quest_end_scripts WHERE datalong IN (48892) AND id=3363;
DELETE FROM quest_end_scripts WHERE datalong IN (6579) AND id=3922;
DELETE FROM quest_end_scripts WHERE datalong IN (48893) AND id=4113;
DELETE FROM quest_end_scripts WHERE datalong IN (48894) AND id=4114;
DELETE FROM quest_end_scripts WHERE datalong IN (48887) AND id=4115;
DELETE FROM quest_end_scripts WHERE datalong IN (48895) AND id=4116;
DELETE FROM quest_end_scripts WHERE datalong IN (48881) AND id=4117;
DELETE FROM quest_end_scripts WHERE datalong IN (18207) AND id=4118;
DELETE FROM quest_end_scripts WHERE datalong IN (17641) AND id=4119;
DELETE FROM quest_end_scripts WHERE datalong IN (48888) AND id=4221;
DELETE FROM quest_end_scripts WHERE datalong IN (48889) AND id=4222;
DELETE FROM quest_end_scripts WHERE datalong IN (48890) AND id=4343;
DELETE FROM quest_end_scripts WHERE datalong IN (48896) AND id=4401;
DELETE FROM quest_end_scripts WHERE datalong IN (48891) AND id=4403;
DELETE FROM quest_end_scripts WHERE datalong IN (44882) AND id=4443;
DELETE FROM quest_end_scripts WHERE datalong IN (48883) AND id=4444;
DELETE FROM quest_end_scripts WHERE datalong IN (48884) AND id=4445;
DELETE FROM quest_end_scripts WHERE datalong IN (48885) AND id=4446;
DELETE FROM quest_end_scripts WHERE datalong IN (48873) AND id=4447;
DELETE FROM quest_end_scripts WHERE datalong IN (48874) AND id=4448;
DELETE FROM quest_end_scripts WHERE datalong IN (48886) AND id=4461;
DELETE FROM quest_end_scripts WHERE datalong IN (48875) AND id=4462;
DELETE FROM quest_end_scripts WHERE datalong IN (48898) AND id=4464;
DELETE FROM quest_end_scripts WHERE datalong IN (48900) AND id=4465;
DELETE FROM quest_end_scripts WHERE datalong IN (48897) AND id=4466;
DELETE FROM quest_end_scripts WHERE datalong IN (48899) AND id=4467;


-- Table `quest_start_scripts` has invalid gameobject (GUID: xx) in SCRIPT_COMMAND_RESPAWN_GAMEOBJECT for script id xx
DELETE FROM quest_start_scripts WHERE datalong IN (44732,44733) AND id=2701;

-- event_scripts` has invalid gameobject (GUID: xxxxx) in SCRIPT_COMMAND_RESPAWN_GAMEOBJECT for script id xxxx
-- in SCRIPT_COMMAND_OPEN_DOOR // in SCRIPT_COMMAND_RESPAWN_GAMEOBJECT // has invalid creature
DELETE FROM event_scripts WHERE datalong IN (16736,16741) AND id=3201;
DELETE FROM event_scripts WHERE datalong IN (16737,16742) AND id=3202;
DELETE FROM event_scripts WHERE datalong IN (16738,16743) AND id=3203;
DELETE FROM event_scripts WHERE datalong IN (16735,16740) AND id=3204;
DELETE FROM event_scripts WHERE datalong IN (16985) AND id=3718;
DELETE FROM event_scripts WHERE datalong IN (27142) AND id=3980;
DELETE FROM event_scripts WHERE datalong IN (9816) AND id=4884;
DELETE FROM event_scripts WHERE datalong IN (21607) AND id=15385;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (103090) AND id=18014;
DELETE FROM event_scripts WHERE datalong IN (73044,73045,73046,73047) AND id=18475;
DELETE FROM event_scripts WHERE datalong IN (72989,72990,72991,72993,72994) AND id=18858;
DELETE FROM event_scripts WHERE datalong IN (183268) AND id=21678;
DELETE FROM event_scripts WHERE datalong IN (72796) AND id=22833;
DELETE FROM event_scripts WHERE datalong IN (72796) AND id=22854;
DELETE FROM event_scripts WHERE datalong IN (72538,72530) AND id=23426;
DELETE FROM event_scripts WHERE datalong IN (72539,72531) AND id=23438;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;
DELETE FROM event_scripts WHERE datalong IN (44882) AND id=17566;

DELETE FROM npc_spellclick_spells WHERE npc_entry=43499;
DELETE FROM npc_spellclick_spells WHERE spell_id=50400;

DELETE FROM creature_questender WHERE id NOT IN (SELECT entry FROM creature_template);
DELETE FROM creature_questender WHERE quest NOT IN (SELECT id FROM quest_template);
DELETE FROM creature_queststarter WHERE id NOT IN (SELECT entry FROM creature_template);
DELETE FROM creature_queststarter WHERE quest NOT IN (SELECT id FROM quest_template);
DELETE FROM creature_loot_template WHERE item=3393;

-----------------------
DELETE FROM creature_loot_template WHERE entry IN (9816,10211,69036,100000);
-----------------------
-- Part 1 

UPDATE creature_template SET lootid=6653 WHERE Entry=6653;
UPDATE creature_template SET lootid=9600 WHERE Entry=9600;
UPDATE creature_template SET lootid=10211 WHERE Entry=10211;
UPDATE creature_template SET lootid=27636 WHERE Entry=27636;
UPDATE creature_template SET lootid=30449 WHERE Entry=30449;
UPDATE creature_template SET lootid=30451 WHERE Entry=30451;
UPDATE creature_template SET lootid=30452 WHERE Entry=30452;
UPDATE creature_template SET lootid=30928 WHERE Entry=30928;
UPDATE creature_template SET lootid=31535 WHERE Entry=31535;
UPDATE creature_template SET lootid=32865 WHERE Entry=32865;
UPDATE creature_template SET lootid=32871 WHERE Entry=32871;
UPDATE creature_template SET lootid=33070 WHERE Entry=33070;
UPDATE creature_template SET lootid=33147 WHERE Entry=33147;
UPDATE creature_template SET lootid=33350 WHERE Entry=33350;
UPDATE creature_template SET lootid=33432 WHERE Entry=33432;
UPDATE creature_template SET lootid=33479 WHERE Entry=33479;
UPDATE creature_template SET lootid=33481 WHERE Entry=33481;
UPDATE creature_template SET lootid=33565 WHERE Entry=33565;
UPDATE creature_template SET lootid=33566 WHERE Entry=33566;
UPDATE creature_template SET lootid=33651 WHERE Entry=33651;
UPDATE creature_template SET lootid=34106 WHERE Entry=34106;
UPDATE creature_template SET lootid=34140 WHERE Entry=34140;
UPDATE creature_template SET lootid=35384 WHERE Entry=35384;
UPDATE creature_template SET lootid=36227 WHERE Entry=36227;
UPDATE creature_template SET lootid=36528 WHERE Entry=36528;
UPDATE creature_template SET lootid=36789 WHERE Entry=36789;
UPDATE creature_template SET lootid=37226 WHERE Entry=37226;
UPDATE creature_template SET lootid=38174 WHERE Entry=38174;
UPDATE creature_template SET lootid=38851 WHERE Entry=38851;
UPDATE creature_template SET lootid=39723 WHERE Entry=39723;
UPDATE creature_template SET lootid=39947 WHERE Entry=39947;
UPDATE creature_template SET lootid=40067 WHERE Entry=40067;
UPDATE creature_template SET lootid=40239 WHERE Entry=40239;
UPDATE creature_template SET lootid=40844 WHERE Entry=40844;
UPDATE creature_template SET lootid=40884 WHERE Entry=40884;
UPDATE creature_template SET lootid=41731 WHERE Entry=41731;
UPDATE creature_template SET lootid=42166 WHERE Entry=42166;
UPDATE creature_template SET lootid=42179 WHERE Entry=42179;
UPDATE creature_template SET lootid=42383 WHERE Entry=42383;
UPDATE creature_template SET lootid=42391 WHERE Entry=42391;
UPDATE creature_template SET lootid=43358 WHERE Entry=43358;
UPDATE creature_template SET lootid=43838 WHERE Entry=43838;
UPDATE creature_template SET lootid=45439 WHERE Entry=45439;
UPDATE creature_template SET lootid=46126 WHERE Entry=46126;
UPDATE creature_template SET lootid=47369 WHERE Entry=47369;
UPDATE creature_template SET lootid=47401 WHERE Entry=47401;
UPDATE creature_template SET lootid=47409 WHERE Entry=47409;
UPDATE creature_template SET lootid=48799 WHERE Entry=48799;
UPDATE creature_template SET lootid=49342 WHERE Entry=49342;
UPDATE creature_template SET lootid=49832 WHERE Entry=49832;
UPDATE creature_template SET lootid=50358 WHERE Entry=50358;
UPDATE creature_template SET lootid=50721 WHERE Entry=50721;
UPDATE creature_template SET lootid=50859 WHERE Entry=50859;
UPDATE creature_template SET lootid=50958 WHERE Entry=50958;
UPDATE creature_template SET lootid=51661 WHERE Entry=51661;
UPDATE creature_template SET lootid=51662 WHERE Entry=51662;
UPDATE creature_template SET lootid=51671 WHERE Entry=51671;
UPDATE creature_template SET lootid=51672 WHERE Entry=51672;
UPDATE creature_template SET lootid=51673 WHERE Entry=51673;
UPDATE creature_template SET lootid=51674 WHERE Entry=51674;
UPDATE creature_template SET lootid=51714 WHERE Entry=51714;
UPDATE creature_template SET lootid=51726 WHERE Entry=51726;
UPDATE creature_template SET lootid=51752 WHERE Entry=51752;
UPDATE creature_template SET lootid=51753 WHERE Entry=51753;
UPDATE creature_template SET lootid=51759 WHERE Entry=51759;
UPDATE creature_template SET lootid=51760 WHERE Entry=51760;
UPDATE creature_template SET lootid=52035 WHERE Entry=52035;
UPDATE creature_template SET lootid=52224 WHERE Entry=52224;
UPDATE creature_template SET lootid=53264 WHERE Entry=53264;
UPDATE creature_template SET lootid=54229 WHERE Entry=54229;
UPDATE creature_template SET lootid=55265 WHERE Entry=55265;
UPDATE creature_template SET lootid=57270 WHERE Entry=57270;
UPDATE creature_template SET lootid=57409 WHERE Entry=57409;
UPDATE creature_template SET lootid=57771 WHERE Entry=57771;
UPDATE creature_template SET lootid=57772 WHERE Entry=57772;
UPDATE creature_template SET lootid=58071 WHERE Entry=58071;
UPDATE creature_template SET lootid=58415 WHERE Entry=58415;
UPDATE creature_template SET lootid=59051 WHERE Entry=59051;
UPDATE creature_template SET lootid=60033 WHERE Entry=60033;
UPDATE creature_template SET lootid=60045 WHERE Entry=60045;
UPDATE creature_template SET lootid=60047 WHERE Entry=60047;
UPDATE creature_template SET lootid=60049 WHERE Entry=60049;
UPDATE creature_template SET lootid=60053 WHERE Entry=60053;
UPDATE creature_template SET lootid=60399 WHERE Entry=60399;
UPDATE creature_template SET lootid=60701 WHERE Entry=60701;
UPDATE creature_template SET lootid=60708 WHERE Entry=60708;
UPDATE creature_template SET lootid=64368 WHERE Entry=64368;
UPDATE creature_template SET lootid=64453 WHERE Entry=64453;
UPDATE creature_template SET lootid=64458 WHERE Entry=64458;
UPDATE creature_template SET lootid=64916 WHERE Entry=64916;
UPDATE creature_template SET lootid=64917 WHERE Entry=64917;
UPDATE creature_template SET lootid=66100 WHERE Entry=66100;
UPDATE creature_template SET lootid=67362 WHERE Entry=67362;
UPDATE creature_template SET lootid=67466 WHERE Entry=67466;
UPDATE creature_template SET lootid=67473 WHERE Entry=67473;
UPDATE creature_template SET lootid=67545 WHERE Entry=67545;
UPDATE creature_template SET lootid=67563 WHERE Entry=67563;
UPDATE creature_template SET lootid=67602 WHERE Entry=67602;
UPDATE creature_template SET lootid=67637 WHERE Entry=67637;
UPDATE creature_template SET lootid=67661 WHERE Entry=67661;
UPDATE creature_template SET lootid=67770 WHERE Entry=67770;
UPDATE creature_template SET lootid=67805 WHERE Entry=67805;
UPDATE creature_template SET lootid=67806 WHERE Entry=67806;
UPDATE creature_template SET lootid=67870 WHERE Entry=67870;
UPDATE creature_template SET lootid=67930 WHERE Entry=67930;
UPDATE creature_template SET lootid=67933 WHERE Entry=67933;
UPDATE creature_template SET lootid=67934 WHERE Entry=67934;
UPDATE creature_template SET lootid=67935 WHERE Entry=67935;
UPDATE creature_template SET lootid=67991 WHERE Entry=67991;
UPDATE creature_template SET lootid=68043 WHERE Entry=68043;
UPDATE creature_template SET lootid=68175 WHERE Entry=68175;
UPDATE creature_template SET lootid=68205 WHERE Entry=68205;
UPDATE creature_template SET lootid=68220 WHERE Entry=68220;
UPDATE creature_template SET lootid=68221 WHERE Entry=68221;
UPDATE creature_template SET lootid=68222 WHERE Entry=68222;
UPDATE creature_template SET lootid=68248 WHERE Entry=68248;
UPDATE creature_template SET lootid=68313 WHERE Entry=68313;
UPDATE creature_template SET lootid=68547 WHERE Entry=68547;
UPDATE creature_template SET lootid=68752 WHERE Entry=68752;
UPDATE creature_template SET lootid=68762 WHERE Entry=68762;
UPDATE creature_template SET lootid=69065 WHERE Entry=69065;
UPDATE creature_template SET lootid=69099 WHERE Entry=69099;
UPDATE creature_template SET lootid=69136 WHERE Entry=69136;
UPDATE creature_template SET lootid=69144 WHERE Entry=69144;
UPDATE creature_template SET lootid=69162 WHERE Entry=69162;
UPDATE creature_template SET lootid=69183 WHERE Entry=69183;
UPDATE creature_template SET lootid=69193 WHERE Entry=69193;
UPDATE creature_template SET lootid=69195 WHERE Entry=69195;
UPDATE creature_template SET lootid=69198 WHERE Entry=69198;
UPDATE creature_template SET lootid=69201 WHERE Entry=69201;
UPDATE creature_template SET lootid=69210 WHERE Entry=69210;
UPDATE creature_template SET lootid=69216 WHERE Entry=69216;
UPDATE creature_template SET lootid=69218 WHERE Entry=69218;
UPDATE creature_template SET lootid=69224 WHERE Entry=69224;
UPDATE creature_template SET lootid=69226 WHERE Entry=69226;
UPDATE creature_template SET lootid=69227 WHERE Entry=69227;
UPDATE creature_template SET lootid=69228 WHERE Entry=69228;
UPDATE creature_template SET lootid=69229 WHERE Entry=69229;
UPDATE creature_template SET lootid=69235 WHERE Entry=69235;
UPDATE creature_template SET lootid=69236 WHERE Entry=69236;
UPDATE creature_template SET lootid=69238 WHERE Entry=69238;
UPDATE creature_template SET lootid=69240 WHERE Entry=69240;
UPDATE creature_template SET lootid=69241 WHERE Entry=69241;
UPDATE creature_template SET lootid=69246 WHERE Entry=69246;
UPDATE creature_template SET lootid=69250 WHERE Entry=69250;
UPDATE creature_template SET lootid=69251 WHERE Entry=69251;
UPDATE creature_template SET lootid=69255 WHERE Entry=69255;
UPDATE creature_template SET lootid=69256 WHERE Entry=69256;
UPDATE creature_template SET lootid=69269 WHERE Entry=69269;
UPDATE creature_template SET lootid=69272 WHERE Entry=69272;
UPDATE creature_template SET lootid=69285 WHERE Entry=69285;
UPDATE creature_template SET lootid=69286 WHERE Entry=69286;
UPDATE creature_template SET lootid=69305 WHERE Entry=69305;
UPDATE creature_template SET lootid=69311 WHERE Entry=69311;
UPDATE creature_template SET lootid=69336 WHERE Entry=69336;
UPDATE creature_template SET lootid=69338 WHERE Entry=69338;
UPDATE creature_template SET lootid=69339 WHERE Entry=69339;
UPDATE creature_template SET lootid=69341 WHERE Entry=69341;
UPDATE creature_template SET lootid=69347 WHERE Entry=69347;
UPDATE creature_template SET lootid=69348 WHERE Entry=69348;
UPDATE creature_template SET lootid=69351 WHERE Entry=69351;
UPDATE creature_template SET lootid=69352 WHERE Entry=69352;
UPDATE creature_template SET lootid=69375 WHERE Entry=69375;
UPDATE creature_template SET lootid=69376 WHERE Entry=69376;
UPDATE creature_template SET lootid=69390 WHERE Entry=69390;
UPDATE creature_template SET lootid=69396 WHERE Entry=69396;
UPDATE creature_template SET lootid=69397 WHERE Entry=69397;
UPDATE creature_template SET lootid=69400 WHERE Entry=69400;
UPDATE creature_template SET lootid=69401 WHERE Entry=69401;
UPDATE creature_template SET lootid=69402 WHERE Entry=69402;
UPDATE creature_template SET lootid=69405 WHERE Entry=69405;
UPDATE creature_template SET lootid=69406 WHERE Entry=69406;
UPDATE creature_template SET lootid=69429 WHERE Entry=69429;
UPDATE creature_template SET lootid=69455 WHERE Entry=69455;
UPDATE creature_template SET lootid=69471 WHERE Entry=69471;
UPDATE creature_template SET lootid=69474 WHERE Entry=69474;
UPDATE creature_template SET lootid=69475 WHERE Entry=69475;
UPDATE creature_template SET lootid=69482 WHERE Entry=69482;
UPDATE creature_template SET lootid=69483 WHERE Entry=69483;
UPDATE creature_template SET lootid=69506 WHERE Entry=69506;
UPDATE creature_template SET lootid=69507 WHERE Entry=69507;
UPDATE creature_template SET lootid=69517 WHERE Entry=69517;
UPDATE creature_template SET lootid=69518 WHERE Entry=69518;
UPDATE creature_template SET lootid=69523 WHERE Entry=69523;
UPDATE creature_template SET lootid=69525 WHERE Entry=69525;
UPDATE creature_template SET lootid=69527 WHERE Entry=69527;
UPDATE creature_template SET lootid=69531 WHERE Entry=69531;
UPDATE creature_template SET lootid=69534 WHERE Entry=69534;
UPDATE creature_template SET lootid=69540 WHERE Entry=69540;
UPDATE creature_template SET lootid=69558 WHERE Entry=69558;
UPDATE creature_template SET lootid=69559 WHERE Entry=69559;
UPDATE creature_template SET lootid=69567 WHERE Entry=69567;
UPDATE creature_template SET lootid=69568 WHERE Entry=69568;
UPDATE creature_template SET lootid=69569 WHERE Entry=69569;
UPDATE creature_template SET lootid=69580 WHERE Entry=69580;
UPDATE creature_template SET lootid=69633 WHERE Entry=69633;
UPDATE creature_template SET lootid=69657 WHERE Entry=69657;
UPDATE creature_template SET lootid=69658 WHERE Entry=69658;
UPDATE creature_template SET lootid=69663 WHERE Entry=69663;
UPDATE creature_template SET lootid=69664 WHERE Entry=69664;
UPDATE creature_template SET lootid=69665 WHERE Entry=69665;
UPDATE creature_template SET lootid=69666 WHERE Entry=69666;
UPDATE creature_template SET lootid=69702 WHERE Entry=69702;
UPDATE creature_template SET lootid=69749 WHERE Entry=69749;
UPDATE creature_template SET lootid=69767 WHERE Entry=69767;
UPDATE creature_template SET lootid=69780 WHERE Entry=69780;
UPDATE creature_template SET lootid=69800 WHERE Entry=69800;
UPDATE creature_template SET lootid=69809 WHERE Entry=69809;
UPDATE creature_template SET lootid=69815 WHERE Entry=69815;
UPDATE creature_template SET lootid=69821 WHERE Entry=69821;
UPDATE creature_template SET lootid=69824 WHERE Entry=69824;
UPDATE creature_template SET lootid=69827 WHERE Entry=69827;
UPDATE creature_template SET lootid=69834 WHERE Entry=69834;
UPDATE creature_template SET lootid=69894 WHERE Entry=69894;
UPDATE creature_template SET lootid=69899 WHERE Entry=69899;
UPDATE creature_template SET lootid=69903 WHERE Entry=69903;
UPDATE creature_template SET lootid=69905 WHERE Entry=69905;
UPDATE creature_template SET lootid=69906 WHERE Entry=69906;
UPDATE creature_template SET lootid=69909 WHERE Entry=69909;
UPDATE creature_template SET lootid=69910 WHERE Entry=69910;
UPDATE creature_template SET lootid=69923 WHERE Entry=69923;
UPDATE creature_template SET lootid=69927 WHERE Entry=69927;
UPDATE creature_template SET lootid=69944 WHERE Entry=69944;
UPDATE creature_template SET lootid=69961 WHERE Entry=69961;
UPDATE creature_template SET lootid=69997 WHERE Entry=69997;
UPDATE creature_template SET lootid=70000 WHERE Entry=70000;
UPDATE creature_template SET lootid=70001 WHERE Entry=70001;
UPDATE creature_template SET lootid=70002 WHERE Entry=70002;
UPDATE creature_template SET lootid=70003 WHERE Entry=70003;
UPDATE creature_template SET lootid=70074 WHERE Entry=70074;
UPDATE creature_template SET lootid=70080 WHERE Entry=70080;
UPDATE creature_template SET lootid=70176 WHERE Entry=70176;
UPDATE creature_template SET lootid=70179 WHERE Entry=70179;
UPDATE creature_template SET lootid=70195 WHERE Entry=70195;
UPDATE creature_template SET lootid=70202 WHERE Entry=70202;
UPDATE creature_template SET lootid=70205 WHERE Entry=70205;
UPDATE creature_template SET lootid=70206 WHERE Entry=70206;
UPDATE creature_template SET lootid=70209 WHERE Entry=70209;
UPDATE creature_template SET lootid=70224 WHERE Entry=70224;
UPDATE creature_template SET lootid=70227 WHERE Entry=70227;
UPDATE creature_template SET lootid=70230 WHERE Entry=70230;
UPDATE creature_template SET lootid=70232 WHERE Entry=70232;
UPDATE creature_template SET lootid=70236 WHERE Entry=70236;
UPDATE creature_template SET lootid=70240 WHERE Entry=70240;
UPDATE creature_template SET lootid=70286 WHERE Entry=70286;
UPDATE creature_template SET lootid=70288 WHERE Entry=70288;
UPDATE creature_template SET lootid=70290 WHERE Entry=70290;
UPDATE creature_template SET lootid=70291 WHERE Entry=70291;
UPDATE creature_template SET lootid=70308 WHERE Entry=70308;
UPDATE creature_template SET lootid=70327 WHERE Entry=70327;
UPDATE creature_template SET lootid=70341 WHERE Entry=70341;
UPDATE creature_template SET lootid=70348 WHERE Entry=70348;
UPDATE creature_template SET lootid=70401 WHERE Entry=70401;
UPDATE creature_template SET lootid=70429 WHERE Entry=70429;
UPDATE creature_template SET lootid=70445 WHERE Entry=70445;
UPDATE creature_template SET lootid=70448 WHERE Entry=70448;
UPDATE creature_template SET lootid=70511 WHERE Entry=70511;
UPDATE creature_template SET lootid=70521 WHERE Entry=70521;
UPDATE creature_template SET lootid=70529 WHERE Entry=70529;
UPDATE creature_template SET lootid=70530 WHERE Entry=70530;
UPDATE creature_template SET lootid=70534 WHERE Entry=70534;
UPDATE creature_template SET lootid=70557 WHERE Entry=70557;
UPDATE creature_template SET lootid=70587 WHERE Entry=70587;
UPDATE creature_template SET lootid=71277 WHERE Entry=71277;
UPDATE creature_template SET lootid=71770 WHERE Entry=71770;
UPDATE creature_template SET lootid=71771 WHERE Entry=71771;
UPDATE creature_template SET lootid=71772 WHERE Entry=71772;
UPDATE creature_template SET lootid=71773 WHERE Entry=71773;
UPDATE creature_template SET lootid=71908 WHERE Entry=71908;
UPDATE creature_template SET lootid=71920 WHERE Entry=71920;
UPDATE creature_template SET lootid=72033 WHERE Entry=72033;
UPDATE creature_template SET lootid=72048 WHERE Entry=72048;
UPDATE creature_template SET lootid=72095 WHERE Entry=72095;
UPDATE creature_template SET lootid=72131 WHERE Entry=72131;
UPDATE creature_template SET lootid=72150 WHERE Entry=72150;
UPDATE creature_template SET lootid=72194 WHERE Entry=72194;
UPDATE creature_template SET lootid=72245 WHERE Entry=72245;
UPDATE creature_template SET lootid=72275 WHERE Entry=72275;
UPDATE creature_template SET lootid=72350 WHERE Entry=72350;
UPDATE creature_template SET lootid=72351 WHERE Entry=72351;
UPDATE creature_template SET lootid=72354 WHERE Entry=72354;
UPDATE creature_template SET lootid=72365 WHERE Entry=72365;
UPDATE creature_template SET lootid=72367 WHERE Entry=72367;
UPDATE creature_template SET lootid=72411 WHERE Entry=72411;
UPDATE creature_template SET lootid=72412 WHERE Entry=72412;
UPDATE creature_template SET lootid=72421 WHERE Entry=72421;
UPDATE creature_template SET lootid=72433 WHERE Entry=72433;
UPDATE creature_template SET lootid=72434 WHERE Entry=72434;
UPDATE creature_template SET lootid=72451 WHERE Entry=72451;
UPDATE creature_template SET lootid=72452 WHERE Entry=72452;
UPDATE creature_template SET lootid=72455 WHERE Entry=72455;
UPDATE creature_template SET lootid=72490 WHERE Entry=72490;
UPDATE creature_template SET lootid=72496 WHERE Entry=72496;
UPDATE creature_template SET lootid=72564 WHERE Entry=72564;
UPDATE creature_template SET lootid=72655 WHERE Entry=72655;
UPDATE creature_template SET lootid=72658 WHERE Entry=72658;
UPDATE creature_template SET lootid=72661 WHERE Entry=72661;
UPDATE creature_template SET lootid=72662 WHERE Entry=72662;
UPDATE creature_template SET lootid=72663 WHERE Entry=72663;
UPDATE creature_template SET lootid=72728 WHERE Entry=72728;
UPDATE creature_template SET lootid=72744 WHERE Entry=72744;
UPDATE creature_template SET lootid=72767 WHERE Entry=72767;
UPDATE creature_template SET lootid=72768 WHERE Entry=72768;
UPDATE creature_template SET lootid=72769 WHERE Entry=72769;
UPDATE creature_template SET lootid=72770 WHERE Entry=72770;
UPDATE creature_template SET lootid=72771 WHERE Entry=72771;
UPDATE creature_template SET lootid=72784 WHERE Entry=72784;
UPDATE creature_template SET lootid=72791 WHERE Entry=72791;
UPDATE creature_template SET lootid=72809 WHERE Entry=72809;
UPDATE creature_template SET lootid=72898 WHERE Entry=72898;
UPDATE creature_template SET lootid=72927 WHERE Entry=72927;
UPDATE creature_template SET lootid=72929 WHERE Entry=72929;
UPDATE creature_template SET lootid=72954 WHERE Entry=72954;
UPDATE creature_template SET lootid=73012 WHERE Entry=73012;
UPDATE creature_template SET lootid=73018 WHERE Entry=73018;
UPDATE creature_template SET lootid=73157 WHERE Entry=73157;
UPDATE creature_template SET lootid=73162 WHERE Entry=73162;
UPDATE creature_template SET lootid=73184 WHERE Entry=73184;
UPDATE creature_template SET lootid=73185 WHERE Entry=73185;
UPDATE creature_template SET lootid=73188 WHERE Entry=73188;
UPDATE creature_template SET lootid=73191 WHERE Entry=73191;
UPDATE creature_template SET lootid=73194 WHERE Entry=73194;
UPDATE creature_template SET lootid=73223 WHERE Entry=73223;
UPDATE creature_template SET lootid=73277 WHERE Entry=73277;
UPDATE creature_template SET lootid=73342 WHERE Entry=73342;
UPDATE creature_template SET lootid=73349 WHERE Entry=73349;
UPDATE creature_template SET lootid=73414 WHERE Entry=73414;
UPDATE creature_template SET lootid=73452 WHERE Entry=73452;
UPDATE creature_template SET lootid=73538 WHERE Entry=73538;
UPDATE creature_template SET lootid=73541 WHERE Entry=73541;
UPDATE creature_template SET lootid=73703 WHERE Entry=73703;
UPDATE creature_template SET lootid=73718 WHERE Entry=73718;
UPDATE creature_template SET lootid=73904 WHERE Entry=73904;
UPDATE creature_template SET lootid=74158 WHERE Entry=74158;
UPDATE creature_template SET lootid=77000 WHERE Entry=77000;
UPDATE creature_template SET lootid=77001 WHERE Entry=77001;
UPDATE creature_template SET lootid=77005 WHERE Entry=77005;
UPDATE creature_template SET lootid=77008 WHERE Entry=77008;
UPDATE creature_template SET lootid=77040 WHERE Entry=77040;
UPDATE creature_template SET lootid=77043 WHERE Entry=77043;
UPDATE creature_template SET lootid=77044 WHERE Entry=77044;
UPDATE creature_template SET lootid=77046 WHERE Entry=77046;
UPDATE creature_template SET lootid=77048 WHERE Entry=77048;
UPDATE creature_template SET lootid=77051 WHERE Entry=77051;
UPDATE creature_template SET lootid=77052 WHERE Entry=77052;
UPDATE creature_template SET lootid=77081 WHERE Entry=77081;
UPDATE creature_template SET lootid=77084 WHERE Entry=77084;
UPDATE creature_template SET lootid=77085 WHERE Entry=77085;
UPDATE creature_template SET lootid=77086 WHERE Entry=77086;
UPDATE creature_template SET lootid=77089 WHERE Entry=77089;
UPDATE creature_template SET lootid=77092 WHERE Entry=77092;
UPDATE creature_template SET lootid=77095 WHERE Entry=77095;
UPDATE creature_template SET lootid=150105 WHERE Entry=150105;

DELETE FROM creature_questender WHERE id>=999999;
DELETE FROM creature_queststarter WHERE id>=999999;
DELETE FROM creature WHERE id>=999999;

-- Remove Old Content
delete from creature where guid in (8304108); -- Duplicate
DELETE FROM creature where id in (901120,901814,91131,91150,525252,91122,91130,600051,67,55,70,91120,91338,600045,600050,91337,1275,95001,5060,901813,91138,91139,91121);
DELETE FROM creature where guid in (8311647,8789931,8789932,78000);
DELETE FROM creature WHERE id IN (SELECT entry FROM creature_template WHERE subname LIKE '%adverse%');
DELETE FROM npc_vendor WHERE entry=78000;
DELETE FROM creature_template WHERE entry>=500002 AND entry<=800084;
DELETE FROM creature WHERE id NOT IN (SELECT entry FROM creature_template);
