-- ChiJi Beacon of hope
DELETE FROM spell_script_names WHERE spell_id = 144475;
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (144475, 'spell_chi_ji_beacon_of_hope');
UPDATE creature_template SET flags_extra = flags_extra | 128, spell1 = 144474 WHERE entry = 71978;

-- ChiJi Beacon of hope-- ChiJi Firestorm
UPDATE creature_template SET unit_flags = unit_flags | (131072 + 2), faction_a = 16, faction_h = 16, flags_extra = flags_extra | 128, minlevel = 90, maxlevel = 90, spell1 = 144463 WHERE entry = 71971;

-- Chi-Ji Speeches
DELETE FROM `creature_text` WHERE `entry` = 71952;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(71952, 0,  0, 'Then let us begin!', 14, 0, 100, 0, 0, 38024,'Chi-Ji Aggro'),
(71952, 1,  0, 'When faced with challenges, the like you have never seen. What do you hope for, what is the future you seek?', 14, 0, 100, 0, 0, 38026,  'Chi-Ji Intro'),
(71952, 2,  0, 'Your hope shines brightly, and even more brightly when you work together to overcome. It will ever light your way in even the darkest of places!', 14, 0, 100, 0, 0, 38025,  'Chi-Ji Death'),
(71952, 3,  0, 'Do not give up on yourself!', 14, 0, 100, 0, 0, 38027,  'Chi-Ji Kill'),
(71952, 4,  0, 'Believe in one another, and let others carry hope for you!', 14, 0, 100, 0, 0, 38028,  'Chi-Ji - Agility'),
(71952, 5,  0, 'Without hope there is no brightness in the future!', 14, 0, 100, 0, 0, 38029,  'Chi-Ji - Chi barrange'),
(71952, 6,  0, 'Create the destiny you seek!', 14, 0, 100, 0, 0, 38030,  'Chi-Ji - Crackling Lightning');

-- ChiJi Child
UPDATE creature_template SET faction_a = 16, faction_h = 16, ScriptName = 'mob_child_of_chi_ji' WHERE entry = 71990;

-- Niuzao speech
DELETE FROM `creature_text` WHERE `entry` = 71954;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(71954, 0,  0, 'We shall see!', 14, 0, 100, 0, 0, 38755,'Niuzao Aggro'),
(71954, 1,  0, 'Can you stand on the tallest peak, winds and sleet buffeting your skin, until the trees wither and the mountains fall into the sea?', 14, 0, 100, 0, 0, 38757,  'Niuzao Intro'),
(71954, 2,  0, 'Though you will be surrounded by foes greater than you can imagine, your fortitude shall allow you to endure. Remember this in the times ahead.', 14, 0, 100, 0, 0, 38756,  'Niuzao Death'),
(71954, 3,  0, 'You must persevere!', 14, 0, 100, 0, 0, 38759,  'Niuzao Kill'),
(71954, 4,  0, 'The winds may be strong and the sleets may sting!', 14, 0, 100, 0, 0, 38759,  'Niuzao - 1'),
(71954, 5,  0, 'You are the mountain, unmovable by all but time!', 14, 0, 100, 0, 0, 38760,  'Niuzao - 2'),
(71954, 6,  0, 'Be vigilant in your stand, or you will never achiefl_charsve your cause!', 14, 0, 100, 0, 0, 38761,  'Niuzao -3 ');

DELETE FROM spell_script_names WHERE spell_id IN (144608, 144610);
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (144608, 'spell_niuzao_charge'), (144610, 'spell_niuzao_headbutt');

UPDATE creature_template SET speed_walk = 2, speed_run = 2, unit_flags = unit_flags | 0x100 WHERE entry IN (71955, 71953, 71954, 71952);